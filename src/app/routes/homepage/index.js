var router = require('express').Router();
var mongoose = require('mongoose');


var Products = require('model/schema').product;


function getProduct(res) {
    Products.find((err, product) => {

        if (err) {
            res.status(500).json(err);
        } else {
          res.render('homepage', { product });
        }
    });
}


/** get homepage */
router.get('/', (req, res) => {
    getProduct(res);
})

/** insert data  */
router.post('/insert', (req, res) => {
    var product = {
        name        : req.body.name,
        nameKhongDau: req.body.nameKhongDau,
        img         : req.body.img,
        cateId      : req.body.cateId,
        des         : req.body.des,
        price       : req.body.price,
        soLuong     : req.body.soLuong
    }
    Products.create(product, (err, product) => {
        if (err) {
            throw err;
        } else {
            getProduct(res);
        }
    })
})

/** update data */
router.put('/update/:id', (req, res) => {
    var product = {
        name        : req.body.name,
        nameKhongDau: req.body.nameKhongDau,
        img         : req.body.img,
        cateId      : req.body.cateId,
        des         : req.body.des,
        price       : req.body.price,
        soLuong     : req.body.soLuong
    }
    Products.update({
        _id: req.params.id
    }, 
    product, 
    function(err, product)  {
            if (err) {
                return res.status(500).json(err);
            } else {
                getProduct(res);
            }
        })
})

/**delete data */
router.delete('/delete/:id', (req, res) => {
    Products.remove({_id    :   req.params.id}, (err, product) => {
        if (err) {
            return res.status(500).json(err);
        } else {
            getProduct(res);
        }
    })
})


// router.get('/', async (req, res, next) => {
//   res.render('homepage');
// });

module.exports = router;
