var mongoose = require('mongoose');
var schema = require('./schema/index');

module.exports = {
  users   : mongoose.model('users', schema.users),
  cart    : mongoose.model('cart', schema.cart),
  infoUser: mongoose.model('infoUser', schema.infoUser),
  product : mongoose.model('product', schema.product) 
}