var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Cart = new Schema({
    name 		:  String,
    email    	: String,
    sdt 		: String,
    maGiaoDich 	: String,
    cart 		: Object,
    soTien      : Number

});

module.exports = Cart;