module.exports = {
  users     : require("./users"),
  infoUser  : require("./infoUser"),
  cart      : require("./cart"),
  product   : require("./product")
};
