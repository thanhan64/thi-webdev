var mongoose = require('mongoose');

var infoUser = new mongoose.Schema({
    fullname: {
        required: true,
        type: String
    },
    code: {
    required: true,
    type: String,
    },
    address: {
    type: String
    },
    email: {
        type: String
    },
    phone: {
        type: Number
    }
})

module.exports = infoUser;